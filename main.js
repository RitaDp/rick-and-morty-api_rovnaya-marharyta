const url = 'https://rickandmortyapi.com/api/character';
let index  = null;
const data = new Array();
fetch(url)
    .then(res => res.json())
    .then(data => {
        const rawData = data.results;
       addCharacters(rawData)
       downloadName(rawData)
        return rawData.map(character => {
        });
    })
    .catch((error) => {
        console.log(JSON.stringify(error));
    });

    function addCharacters(character) {

    if (history.scrollRestoration) {
        history.scrollRestoration = "manual";
    } else {
        window.onbeforeunload = function () {
            window.scrollTo(0, 0);
        }
    }

    data.splice(0, 1, character)
        const container = document.getElementById("container")
    
        for (index = 0; index < 10; index++) {
          
            const char = document.createElement("div");
            char.className = "characters";
            char.id = character[index].id;
            
            let person = "<H1>" + character[index].name + "</H1>"
                + "<img src=\"" + character[index].image + "\">"
                + "<H2>" + character[index].species + "</H2>"
                + "<p>" + character[index].location.name + "</p>"
                + "<a>" + "Location: " + character[index].location.url + "</a>";
            
            character[index].episode.map(e => {
                const episode = "<div class=\"epi\">" + "Episode: " + e + "</div>\n"
                person = person + episode
            })
            person = person + "<time>" + "Created: " + character[index].created + "</time>"+
            "<button " + "id=" + character[index].id + " onClick=" + "\"remove(this.id)\"" + " class=\"clickDelete\">" + "Delete" + "</button>"; 
            char.innerHTML = person
            container.appendChild(char);
            
        }
    }
    
    window.addEventListener('scroll', function() {
        const {scrollTop, scrollHeight, clientHeight}
        = this.document.documentElement;

        if(clientHeight + scrollTop >= scrollHeight){
            moreLoad()
        }
      });

    function moreLoad (){
        let character = data[0]

        if(character === null 
            || index === null
            || index >= character.length){
            return
        }
       
        let count;
        if(index + 10 > character.length){
            count = (character.length - index)
        }else{
            count = 10 + index;
        }

        for (index; index < count; index++){
            const char = document.createElement("div");                
            char.className = "characters";
            char.id = character[index].id;
            
            let person = "<H1>" + character[index].name + "</H1>"
                + "<img src=\"" + character[index].image + "\">"
                + "<H2>" + character[index].species + "</H2>"
                + "<p>" + character[index].location.name + "</p>"
                + "<a>" + "Location: " + character[index].location.url + "</a>";
            
            character[index].episode.map(e => {
                const episode = "<div class=\"epi\">" + "Episode: " + e + "</div>\n"
                person = person + episode
            })

            person = person + "<time>" + "Created: " + character[index].created + "</time>"+
            "<button " + "id=" + character[index].id + " onClick=" + "\"remove(this.id)\"" + " class=\"clickDelete\">" + "Delete" + "</button>";  
            
            char.innerHTML = person
            container.appendChild(char);
            
        }
    }

    function sortByDateUp(){
        let character = data[0]
        let sorting = character.sort((a,b) => {
            return new Date(a.created) - new Date(b.created)
      })
      document.getElementById("container").innerHTML = "";
      addCharacters(sorting)    
    }

    function sortByDateDown (){
        let character = data[0]
        let sorting = character.sort((a,b) =>{
            return new Date (b.created) - new Date (a.created)
        })
        document.getElementById("container").innerHTML = "";
        addCharacters(sorting)

    }

    function sortByEpisodes() {
        let character = data[0]
        let sorting = character.sort((a,b) =>{
        if((b.episode.length - a.episode.length) === 0){
            return new Date(b.created) - new Date(a.created) 
        }
          return b.episode.length - a.episode.length
        })
        document.getElementById("container").innerHTML = "";
        addCharacters(sorting)
    }

    function remove (id) {       
       document.getElementById(id).remove()
       let character = data[0];
       character.map(e=>{
            if(e.id == id){ 
                data[0].splice(character.indexOf(e), 1)
            }
        })
        
    }

    function topButton (){
       let character = data[0]
       document.getElementById("container").innerHTML = "";
       addCharacters(character)
       window.scrollTo(0, 0);
    }

    function downloadName(character){
        character.map(e =>{
            let name = e.name;
            let id = e.id
            var person = {name,id}
        })
    }
      
    document.getElementById("search").addEventListener('input', (event) => {
        let character = data[0]
        index = character.length
        search_term = event.target.value.toLowerCase();
        document.getElementById("container").innerHTML = ""
        character.forEach(element => {
            if (element.name.toLowerCase().startsWith(search_term)){      
                addCharacter(element)  
        }
    })
});

    function addCharacter(character){
         const char = document.createElement("div");
          
            char.className = "characters";
            char.id = character.id;
            
            let person = "<H1>" + character.name + "</H1>"
                + "<img src=\"" + character.image + "\">"
                + "<H2>" + character.species + "</H2>"
                + "<p>" + character.location.name + "</p>"
                + "<a>" + "Location: " + character.location.url + "</a>";
            
            character.episode.map(e => {
                const episode = "<div class=\"epi\">" + "Episode: " + e + "</div>\n"
                person = person + episode
            })

            person = person + "<time>" + "Created: " + character.created + "</time>"+
            "<button " + "id=" + character.id + " onClick=" + "\"remove(this.id)\"" + " class=\"clickDelete\">" + "Delete" + "</button>";  
            
            char.innerHTML = person
            container.appendChild(char);
    }